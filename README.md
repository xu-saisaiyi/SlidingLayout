﻿# SlidingLayout

SlidingLayout是一种Component控件，可以帮助你实现类似微信网页浏览的下拉功能。

SlidingLayout简单易用，完美所有Component组件,包括ListContainer、ScrollView等等。

## 效果图
<img src="https://gitee.com/HarmonyOS-tpc/SlidingLayout/raw/master/screenshot/sliding1.gif" width="30%">
<br/>
<br/>
<img src="https://gitee.com/HarmonyOS-tpc/SlidingLayout/raw/master/screenshot/sliding2.gif" width="30%">


## 导入项目

方法一：  
通过library生成har包，添加har包到libs文件夹内
在entry的gradle内添加如下代码
```gradle
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

方法二：
```gradle
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:SlidingLayout:1.0.2'
```


## 用法

SlidingLayout的使用非常简单，你只需要将你想实现的控件在XML布局中嵌套进SlidingLayout即可，如你需要让ListContainer实现果冻效果：

### 1.创建背景Component的xml

``` xml
<?xml version="1.0" encoding="utf-8"?>
<Text
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        ohos:width="match_parent"
        ohos:height="match_parent"
        ohos:background_element="#8c8c8e"
        ohos:text_alignment="top"
        ohos:text_size="12fp"
        ohos:text_color="#f5f3f3"
        ohos:padding="16vp"
        ohos:text="developed by HomhomLin"/>
```

### 2.将你的控件放进SlidingLayout中

注意布局需要res-auto命名空间，注意将自己的控件设置一个背景，否则会将背景Component透视出来。

```xml
<?xml version="1.0" encoding="utf-8"?>
<lib.homhomlib.design.SlidingLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        xmlns:hap="http://schemas.huawei.com/res-auto"
        ohos:width="match_parent"
        ohos:height="match_parent"
        hap:sliding_mode="0"
        hap:background_component="$layout:view_bg">


    <ListContainer
            ohos:id="$+id:listcontainer"
            ohos:width="match_parent"
            ohos:height="match_parent"
            ohos:background_element="#ffffff"
            ohos:visibility="visible">
    </ListContainer>

</lib.homhomlib.design.SlidingLayout>
```

### 3.没有第三步啦！

运行即可看到效果！具体其他的内容可以看Demo。

## XML样式参数

 * `background_component` 背景component
 * `sliding_mode` 滑动模式，0为上下可弹跳，1为顶部弹跳，2为底部弹跳，默认为0
 * `top_max` 当滑动模式为top时才有效，用于可滑动的最大距离，如"top_max:200dp"，默认为-1（不限制）

## 常用API

 * `public void setSlidingOffset(float slidingOffset)` 设置控件的滑动阻力，有效值为0.1F~1.0F，值越小阻力越大，默认为0.5F
 * `public void setTargetComponent(Component component)` 设置控件的前景Component
 * `public void setBackgroundComponent(Component component)` 设置控件的背景Component
 * `public void setSlidingListener(SlidingListener slidingListener)` 给控件设置监听，可以监听滑动情况
 * `public void setSlidingMode(int mode)` 设置滑动模式
 * `public void setSlidingDistance(int max)` 设置最大滑动距离，仅在top模式下有效


## Developed By

 * Linhonghong - <linhh90@163.com>

## License
Copyright 2016 LinHongHong

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.