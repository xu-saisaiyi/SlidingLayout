/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.homhomlib.design;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.RecycleItemProvider;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.HashMap;
import java.util.List;

/**
 * 适配器
 *
 * @param <T> 数据类型
 */
public abstract class EasyListProvider<T> extends RecycleItemProvider {
    private Context context;
    private List<T> data;
    private int mLayoutId;

    /**
     * 构造函数
     *
     * @param context   上下文
     * @param mLayoutId 条目的资源文件id
     * @param data      数据源
     */
    public EasyListProvider(Context context, int mLayoutId, List<T> data) {
        this.context = context;
        this.mLayoutId = mLayoutId;
        this.data = data;
    }

    /**
     * 设置数据
     *
     * @param data 数据
     */
    public void setData(List<T> data) {
        this.data = data;
        notifyDataChanged();
    }

    /**
     * \
     * 返回数据
     *
     * @return 数据
     */
    public List<T> getData() {
        return data;
    }

    @Override
    public int getCount() {
        return data != null ? data.size() : 0;
    }

    @Override
    public T getItem(int position) {
        return data != null ? data.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer parent) {
        ViewHolder viewHolder;
        if (convertComponent != null) {
            viewHolder = (ViewHolder) convertComponent.getTag();
        } else {
            convertComponent = LayoutScatter.getInstance(context).parse(mLayoutId, null, false);
            viewHolder = new ViewHolder(convertComponent);
            convertComponent.setTag(viewHolder);
        }
        bind(viewHolder, getItem(position), position);
        return convertComponent;
    }

    /**
     * bangding
     *
     * @param holder   持有component
     * @param data     数据
     * @param position 位置
     */
    protected abstract void bind(ViewHolder holder, T data, int position);

    /**
     * 持有者
     */
    protected static class ViewHolder {
        /**
         * 每个item的UI
         */
        public Component itemView;
        private HashMap<Integer, Component> mViews = new HashMap<>();


        ViewHolder(Component component) {
            this.itemView = component;
        }

        /**
         * 设置文字
         *
         * @param viewId viewId
         * @param text   文字
         * @return 持有
         */
        public ViewHolder setText(int viewId, String text) {
            ((Text) getView(viewId)).setText(text);
            return this;
        }

        /**
         * 获取控件
         *
         * @param viewId 控件Id
         * @param <E>    Component
         * @return Component
         */
        public <E extends Component> E getView(int viewId) {
            E view = (E) mViews.get(viewId);
            if (view == null) {
                view = (E) itemView.findComponentById(viewId);
                mViews.put(viewId, view);
            }
            return view;
        }

    }

}
