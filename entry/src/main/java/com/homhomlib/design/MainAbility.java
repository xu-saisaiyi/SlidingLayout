package com.homhomlib.design;

import lib.homhomlib.design.SlidingLayout;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * 主界面
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        Button listButton = (Button) findComponentById(ResourceTable.Id_btnListView);
        SlidingLayout slidingLayout = (SlidingLayout) findComponentById(ResourceTable.Id_slidinglayout);
        listButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(ListContainerAbility.class.getCanonicalName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }
}
