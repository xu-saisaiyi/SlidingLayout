/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.homhomlib.design;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;

/**
 * list界面
 */
public class ListContainerAbility extends Ability {
    ListContainer mListContainer;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_listcontainer);
        mListContainer = (ListContainer) findComponentById(ResourceTable.Id_listcontainer);
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            integers.add(i);
        }
        mListContainer.setItemProvider(new EasyListProvider<Integer>(getContext(),
                ResourceTable.Layout_list_item, integers) {
            @Override
            protected void bind(ViewHolder holder, Integer data, int position) {
                holder.setText(ResourceTable.Id_text, "Sliding" + data);
            }
        });
    }
}
